const { createApp } = Vue
createApp({
    data() {
      return {
         buttom:[
          'A','<<' ,'>>','CE','del',
          'B','('  ,')' ,'%' ,'÷',
          'C','7'  ,'8' ,'9' ,'×',
          'D','4'  ,'5' ,'6' ,'-',
          'E','1'  ,'2' ,'3' ,'+',
          'F','+/-','0' ,'.' ,'='
        ],
        num:0,
        inputNow:0
      }
    },
    methods: {
      input(key)
      {
        if(this.num < 4294967295) //判断是否超出
        {switch (this.inputNow) { //输入数字
          case 0:
            if(['A','B','C','D','E','F','1','2','3','4','5','6','7','8','9','0'].includes(key))
            {
              this.insert(key)
            }
            break;

          case 1:
            if(['1','2','3','4','5','6','7','8','9','0'].includes(key))
            {
              this.insert(key)
            }
            break;

          case 2:
          if(['1','2','3','4','5','6','7','0'].includes(key))
          {
            this.insert(key)
          }
          break;

          case 3:
          if(['1','0'].includes(key))
          {
            this.insert(key)
          }
          break;

          default:
            break;
        }}

        switch (key) {
          case 'del'://删除键
            switch (this.inputNow) {
              case 0:
                if(this.num.toString(16).length == 1)
                  this.num = 0
                else
                  this.num = parseInt(this.num.toString(16).slice(0,-1), 16)
                break;   
              case 1:
                if(this.num.toString(10).length == 1)
                  this.num = 0
                else
                  this.num = parseInt(this.num.toString(10).slice(0,-1), 10)
                break;
              case 2:
                if(this.num.toString(8).length == 1)
                  this.num = 0
                else
                  this.num = parseInt(this.num.toString(8).slice(0,-1), 8)
                break;   
              case 3:
                if(this.num.toString(2).length == 1)
                  this.num = 0
                else
                  this.num = parseInt(this.num.toString(2).slice(0,-1), 2)
                break;
            }
            break;
          
            case 'CE':
              this.num = 0
              break;
        
          default:
            break;
        }
      },
      insert(key)
      {
        switch (this.inputNow) {
          case 0:
            this.num = parseInt(this.num.toString(16) + key.toString(16), 16)
            break;
          
          case 1:
            this.num = parseInt(this.num.toString(10) + key.toString(10), 10)
            break;

          case 2:
            this.num = parseInt(this.num.toString(8) + key.toString(8), 8)
            break;

          case 3:
            this.num = parseInt(this.num.toString(2) + key.toString(2), 2)
            break;
          default:
            break;
        }
      }
    },
    mounted() {
        window.close = this.close;
        window.openapp = this.openapp
    }
  }).mount('#app')
