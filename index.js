const { createApp } = Vue
  
  createApp({
    data() {
      return {
        menuShow:true,
        picList:[
          {id:5,pic:'1.jpg'},
          {id:5,pic:'1.jpg'}, 
          {id:5,pic:'1.jpg'},
          {id:5,pic:'1.jpg'},     
                ],
        appList:[
          {name:'课程表',ico:'bi-calendar2-week-fill',url:'kebiao',color:'skyblue'},
          {name:'成绩单',ico:'bi-clipboard-data-fill',url:'chengji',color:'#29c25c'},
          {name:'登录',ico:'bi-arrow-right-square-fill',url:'login',color:'#a65fec'},
          {name:'查空教室',ico:'bi-building-fill-check',url:'jiaoshi',color:'#e6cd3e'},
          {name:'下载app',ico:'bi-arrow-down-circle-fill',url:'xiazai',color:'#4CAAE4'},
          {name:'商务合作',ico:'bi-piggy-bank-fill',url:'hezuo',color:'#ff9300'},
          {name:'考场查询',ico:'bi-newspaper',url:'kaoshi',color:'#EFC3CA'},
          {name:'进制计算器',ico:'bi-calculator-fill',url:'jisuanqi',color:'#39D0D5'},
                ],
        usedApp:JSON.parse(localStorage.getItem('usedApp')),
        appnow:-1,
        mainapp:[0,1,6,4],
        opendapp:[],
        picnow:0
        
      }
    },
    methods: {
        start(){
          this.timer = setInterval(this.picChange, 3000)
        },
        picChange()
        {
          this.picnow++
          if(this.picnow == 4)
            this.picnow = 0
        },
        close(index,from)
        {
          this.openapp(from)
          setTimeout(() => this.opendapp.splice(this.opendapp.indexOf(index),1), 300)
          
        },
        openapp(index,from)
        {
          index = index - 0
          from = from - 0
          if(index != -1)
          {
            this.addused(index)
            if(this.opendapp.indexOf(index)==-1)
              this.opendapp.push(index)
              this.appList[index].url = this.appList[index].url.split('?')[0]
            if(from == -2)
              this.appList[index].url = this.appList[index].url+'?from='+this.appnow
            else
              this.appList[index].url = this.appList[index].url+'?from='+from
            this.menuShow=false
            setTimeout(() => this.appnow=index, 50)
          }
          else
          {
            this.appnow=index
          }
          
          
        },
        addused(index)
        {
          index = index - 0
          if(this.usedApp==null)
          {
            this.usedApp = []
          }
          if(this.usedApp.indexOf(index)!=-1)
            this.usedApp.splice(this.usedApp.indexOf(index),1)
            this.usedApp.unshift(index)
          if(this.usedApp.length > 4)
            this.usedApp = this.usedApp.slice(0,4)
          localStorage.setItem('usedApp',JSON.stringify(this.usedApp))
          
        }
      },
        
    mounted() {
       window.close = this.close;
       window.openapp = this.openapp
       this.start()
       setTimeout(() => this.menuShow=false, 3000)
    }
  }).mount('#app')