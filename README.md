### 介绍
莆田学院的非官方掌上教务系统，用于校外网络查询教务信息。
[点击前往试用](http://edu.zhoujump.club "点击前往")
### 部署
#####呃，因为是自用并没有想过其他院校的适配，如果你所在学校也是使用方正教务系统，也许你可以按照以下步骤部署试试。
1. 安装nginx
1. `systemctl start nginx` 启动nginx
1. `cd /user/share/nginx/` 进入nginx目录
1. `git clone https://gitlab.com/zhoujump/jiaowu.git` 克隆文件
1. `vim /etc/nginx/nginx.conf` 修改配置文件
```xml
server {
        listen        80;
        server_name  localhost;
        root /www/admin/localhost_80/wwwroot/ ;

	location  /jwglxt/ {//此处需要根据你的学校教务网址修改
        proxy_pass  http://172.16.9.205;
	proxy_hide_header X-Frame-Options; 
	add_header X-Frame-Options ALLOWALL;
      }

	location  /zftal-ui-v5-1.0.2/ {
        proxy_pass  http://172.16.9.205;
	proxy_hide_header X-Frame-Options; 
	add_header X-Frame-Options ALLOWALL;
      }

        location / {
            root /www/admin/localhost_80/wwwroot/;
            index  index.php index.html error/index.html;
        }
       }
```
1. `systemctl restart nginx` 重启nginx
#####接下来是修改源代码，因为我没有想过做适配
1. 将所有’edu.zhoujump.club‘替换为你的域名或者IP
1. 将所有’/jwglxt/‘替换为你学校教务的目录
1. 不出意外的话它现在已经可以运行了
